CRM Core Demo Standard Fields
-----------------------------
CRM Core Demo Standard Fields is a simple feature that adds a set of fields 
to contact records in CRM Core. It is used primarily for testing new features 
in CRM Core against a set of common fields, in order to evaluate how the 
feature works. It also provides some logical default fields that can be used 
for evaluating and demoing CRM Core.

It is not necessary to use the standard fields as part of your CRM Core 
installation, but feel free to do so.

Installation
------------
1) Place this module in your site's modules directory 
  (i.e. sites/all/modules)

2) Enable from the features menu.

Maintainers
-----------
- techsoldaten
- RoSk0

Sponsored by Trellon
